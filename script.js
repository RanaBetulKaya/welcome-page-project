function createWorkElement(img1,img2,img3,title,description){
    const productInfo=document.createElement('div');
    productInfo.classList.add('row');
    productInfo.innerHTML = generateProductHTML(
        img1,
        img2,
        img3,
        title,
        description
    );
    document
    .querySelector("#workPage")
    .appendChild(productInfo);
}
function generateProductHTML(img1,img2,img3,title,description){
    return  `
    <div id="carouselExampleAutoplaying" class="carousel carousel-dark slide col-lg-6 col-sm-12 mt-5" data-bs-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="${img1}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="${img2}" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
                <img src="${img3}" class="d-block w-100" alt="...">
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-sm-12 mt-5" id="workTextDiv" >
        <h1>${title}</h1>
        <p>${description}</p>
    </div>
    `;
}
createWorkElement(
    './img/product.jpg',
    './img/banking.jpg',
    './img/app.jpg',
    'uApp',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
);
createWorkElement(
    './img/banking.jpg',
    './img/product.jpg',
    './img/app.jpg',
    'CBank App',
    'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
);